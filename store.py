import time


class Store:
    def __init__(self, external_store, get_try_count=None, get_try_delay=10,
                 cache_try_count=None, cache_try_delay=1):
        self.external_store = external_store
        self.get_try_count = get_try_count
        self.get_try_delay = get_try_delay
        self.cache_try_count = cache_try_count
        self.cache_try_delay = cache_try_delay

    def cache_get(self, key):
        try_count = self.cache_try_count or 1
        while try_count:
            try:
                return self.external_store.get(name=key)
            except Exception:
                try_count -= 1
                if not try_count:
                    return
                time.sleep(self.cache_try_delay)

    def cache_set(self, key: str, value, ttl: int):
        try_count = self.cache_try_count or 1
        while try_count:
            try:
                self.external_store.set(name=key, value=value, ex=ttl)
                return
            except Exception:
                try_count -= 1
                if not try_count:
                    return
                time.sleep(self.cache_try_delay)

    def get(self, key):
        try_count = self.get_try_count or 1
        while try_count:
            try:
                return self.external_store.get(name=key)
            except Exception as e:
                try_count -= 1
                if not try_count:
                    raise e
                time.sleep(self.get_try_delay)
