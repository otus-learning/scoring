from datetime import datetime


def rewind_year(years: int, from_date=datetime.now().date()):
    target_year = from_date.year + years
    try:
        return from_date.replace(year=target_year)
    except ValueError:
        return from_date.replace(day=28, year=target_year)
