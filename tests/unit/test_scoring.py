from datetime import datetime
from unittest.mock import Mock

import pytest

from scoring import get_score, get_interests


def test_get_score_from_cache():
    store_mock = Mock()
    store_mock.cache_get = Mock(return_value='3.0')
    result = get_score(store=store_mock, email='sssss@gmail.com', phone='+789897')
    assert result == 3.0
    store_mock.cache_get.assert_called_once()


def test_get_score_from_bad_store(bad_store_object):
    result = get_score(store=bad_store_object, email='sssss@gmail.com', phone='+789897')
    assert result == 3.0


def test_get_interests_from_bad_store(bad_store_object):
    with pytest.raises(Exception):
        get_interests(bad_store_object, 123)


@pytest.mark.parametrize(
    'data, expected_result',
    [
        ({'email': 'sssss@gmail.com', 'phone': '+789897'}, 3.0),
        ({'email': 'sssss@gmail.com', 'phone': '+789897', 'birthday': datetime.today()}, 3.0),
        ({'email': 'sssss@gmail.com', 'phone': '+789897', 'birthday': datetime.today(), 'gender': 0}, 3.0),
        ({
             'email': 'sssss@gmail.com', 'phone': '+789897', 'birthday': datetime.today(), 'gender': 0,
             'first_name': 'Имя'
         }, 3.0),
        ({
             'email': 'sssss@gmail.com', 'phone': '+789897', 'birthday': datetime.today(),
             'gender': 0, 'first_name': 'Имя', 'last_name': 'Фамилия'
         }, 3.5),
    ]
)
def test_get_score_no_cache(data, expected_result):
    store_mock = Mock()
    store_mock.cache_get = Mock(return_value=None)
    result = get_score(store=store_mock, **data)
    assert result == expected_result
    store_mock.cache_get.assert_called_once()
