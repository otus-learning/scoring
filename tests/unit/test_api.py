import datetime
from unittest import mock

import pytest

import api


@pytest.mark.parametrize(
    'value, min_length',
    [
        ({'key': 'value'}, None),
        ('это строка, а не список', None),
        (123, None),
        (True, None),
        (None, None),
        ([123, 456], 5)
    ]
)
def test_list_field_invalid(value, min_length):
    with pytest.raises(api.ValidationError):
        api.List(min_length=min_length).deserialize(value=value)


@pytest.mark.parametrize(
    'value, min_length',
    [
        ([123, 658768], None),
        ([123, 456, 5, 5745], 3)
    ]
)
def test_list_field_valid(value, min_length):
    result = api.List(min_length=min_length).deserialize(value=value)
    assert isinstance(result, list)


@pytest.mark.parametrize(
    'value',
    [
        {'key': 'value'},
        ['это список', 'а не строка'],
        123,
        True,
        None
    ]
)
def test_char_field_invalid(value):
    with pytest.raises(api.ValidationError):
        api.CharField().deserialize(value=value)


def test_char_field_valid():
    result = api.CharField().deserialize(value='это строка 1585765')
    assert isinstance(result, str)


@pytest.mark.parametrize(
    'value',
    [
        {'key': 'value'},
        ['это список', 'а не строка'],
        123,
        True,
        None,
        'эта строка не похожа на электронный адрес'
    ]
)
def test_email_field_invalid(value):
    with pytest.raises(api.ValidationError):
        api.EmailField().deserialize(value=value)


def test_email_field_valid():
    result = api.CharField().deserialize(value='aaaaa@gmail.com')
    assert isinstance(result, str)


@pytest.mark.parametrize(
    'value',
    [
        {'key': 'value'},
        ['это список', 'а не строка'],
        123,
        True,
        None,
        'эта строка не похожа на электронный адрес',
        '+45487',
        '+75487985698658975987',
        'ggggg@gmail.com',
    ]
)
def test_phone_field_invalid(value):
    with pytest.raises(api.ValidationError):
        api.PhoneField().deserialize(value=value)


def test_phone_field_valid():
    result = api.PhoneField().deserialize(value='79175002040')
    assert isinstance(result, str)


@pytest.mark.parametrize(
    'value',
    [
        {'key': 'value'},
        ['это список', 'а не строка'],
        123,
        True,
        None,
        'эта строка не похожа на электронный адрес',
        '+45487',
        '+75487985698658975987',
        'ggggg@gmail.com',
    ]
)
def test_date_field_invalid(value):
    with pytest.raises(api.ValidationError):
        api.DateField().deserialize(value=value)


def test_date_field_valid():
    result = api.DateField().deserialize(value='22.01.1910')
    assert result == datetime.date(1910, 1, 22)


@pytest.mark.parametrize(
    'value',
    [
        {'key': 'value'},
        ['это список', 'а не строка'],
        123,
        True,
        None,
        'эта строка не похожа на электронный адрес',
        '+45487',
        '+75487985698658975987',
        'ggggg@gmail.com',
        '01.01.1910'
    ]
)
def test_birth_day_field_invalid(value):
    with pytest.raises(api.ValidationError):
        api.BirthDayField().deserialize(value=value)


def test_birth_day_field_valid():
    result = api.BirthDayField().deserialize(value='22.01.2022')
    assert result == datetime.date(2022, 1, 22)


@pytest.mark.parametrize(
    'value',
    [
        {'key': 'value'},
        ['это cмешаный список', 'а не список интов', 1],
        123,
        True,
        None,
        'эта строка не похожа на электронный адрес',
        '+45487',
        '+75487985698658975987',
        'ggggg@gmail.com',
        '01.01.1910',
    ]
)
def test_client_ids_field_invalid(value):
    with pytest.raises(api.ValidationError):
        api.ClientIDsField().deserialize(value=value)


def test_client_ids_field_valid():
    result = api.ClientIDsField().deserialize(value=[1, 2, 435456])
    assert result == [1, 2, 435456]


def test_empty_request_body():
    _, code = api.method_handler(request={'body': {}}, ctx={}, store={})
    assert code == api.INVALID_REQUEST


@pytest.mark.parametrize(
    'body',
    [{"account": "horns&hoofs", "login": "h&f", "method": "online_score", "token": "", "arguments": {}},
     {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "token": "sdd", "arguments": {}},
     {"account": "horns&hoofs", "login": "admin", "method": "online_score", "token": "", "arguments": {}}]
)
def test_bad_auth(body):
    _, code = api.method_handler(request={'body': body}, ctx={}, store={})
    assert code == api.FORBIDDEN


@pytest.mark.parametrize(
    'body',
    [{"account": "horns&hoofs", "login": "h&f", "method": "online_score"},
     {"account": "horns&hoofs", "login": "h&f", "arguments": {}},
     {"account": "horns&hoofs", "method": "online_score", "arguments": {}}]
)
def test_invalid_method_request(check_auth_mock_mocker, body):
    body['token'] = 'is_token'
    response, code = api.method_handler(request={'body': body}, ctx={}, store={})
    assert code == api.INVALID_REQUEST
    assert len(response)


@pytest.mark.parametrize(
    'method, arguments,',
    [
        ("online_score", {}),
        ("online_score", {"phone": "79175002040"}),
        ("online_score", {"phone": "89175002040", "email": "stupnikov@otus.ru"}),
        ("online_score", {"phone": "79175002040", "email": "stupnikovotus.ru"}),
        ("online_score", {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": -1}),
        ("online_score", {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": "1"}),
        ("online_score", {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.1890"}),
        ("online_score", {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "XXX"}),
        ("online_score",
         {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.2000", "first_name": 1}),
        ("online_score",
         {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.2000",
          "first_name": "s", "last_name": 2}),
        ("online_score", {"phone": "79175002040", "birthday": "01.01.2000", "first_name": "s"}),
        ("online_score", {"email": "stupnikov@otus.ru", "gender": 1, "last_name": 2}),
        ('clients_interests', {"date": "20.07.2017"}),
        ('clients_interests', {"client_ids": [], "date": "20.07.2017"}),
        ('clients_interests', {"client_ids": {1: 2}, "date": "20.07.2017"}),
        ('clients_interests', {"client_ids": ["1", "2"], "date": "20.07.2017"}),
        ('clients_interests', {"client_ids": [1, 2], "date": "XXX"}),
    ]
)
def test_invalid_request(check_auth_mock_mocker, method, arguments):
    body = {
        "account": "horns&hoofs", "login": "h&f", "token": "is_token", "method": method, "arguments": arguments
    }
    response, code = api.method_handler(request={'body': body}, ctx={}, store={})
    assert code == api.INVALID_REQUEST
    assert len(response)
