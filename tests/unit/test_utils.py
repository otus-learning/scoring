import datetime

import pytest

from utils import rewind_year


@pytest.mark.parametrize(
    'date_from, years, expected_date',
    [
        (datetime.datetime(2021, 7, 27), -1, datetime.datetime(2020, 7, 27)),
        (datetime.datetime(2020, 2, 29), -2, datetime.datetime(2018, 2, 28)),
        (datetime.datetime(2020, 2, 29), 4, datetime.datetime(2024, 2, 29)),
    ]
)
def test_rewind_year(date_from, years, expected_date):
    result_date = rewind_year(years=years, from_date=date_from)
    assert result_date == expected_date
