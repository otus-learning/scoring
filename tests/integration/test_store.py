import time

from tests.conftest import check_redis_host_for_test


@check_redis_host_for_test
def test_store_get_after_disconnect(redis_instance_for_tests, store_object_for_tests):
    redis_instance_for_tests.connection_pool.disconnect()
    result = store_object_for_tests.get('i:1')
    assert len(result) > 0


@check_redis_host_for_test
def test_store_cache_set_and_get(redis_instance_for_tests, store_object_for_tests):
    key = 'key_test_store_cache_set_and_get'
    value = 'value_test_store_cache_set_and_get'
    store_object_for_tests.cache_set(key=key, value=value, ttl=5)
    assert store_object_for_tests.cache_get(key=key) == value


@check_redis_host_for_test
def test_store_cache_ttl(redis_instance_for_tests, store_object_for_tests):
    key = 'key_test_store_cache_set_and_get'
    value = 'value_test_store_cache_set_and_get'
    store_object_for_tests.cache_set(key=key, value=value, ttl=1)
    time.sleep(2)
    assert store_object_for_tests.cache_get(key=key) is None
