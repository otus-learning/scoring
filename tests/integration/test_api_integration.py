import datetime

import pytest

import api
from tests.conftest import check_redis_host_for_test


@pytest.mark.parametrize(
    'arguments',
    [{"phone": "79175002040", "email": "stupnikov@otus.ru"},
     {"phone": 79175002040, "email": "stupnikov@otus.ru"},
     {"gender": 1, "birthday": "01.01.2000", "first_name": "a", "last_name": "b"},
     {"gender": 0, "birthday": "01.01.2000"},
     {"gender": 2, "birthday": "01.01.2000"},
     {"first_name": "a", "last_name": "b"},
     {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.2000",
     "first_name": "a", "last_name": "b"}]
)
@check_redis_host_for_test
def test_ok_score_request(store_object_for_tests, check_auth_mock_mocker, arguments):
    body = {
        "account": "horns&hoofs", "login": "h&f", "method": "online_score", 'token': 'is_token', "arguments": arguments
    }
    response, code = api.method_handler(request={'body': body}, ctx={}, store=store_object_for_tests)
    assert code == api.OK
    score = response.get("score")
    assert isinstance(score, (int, float))
    assert score >= 0


@check_redis_host_for_test
def test_ok_score_admin_request(store_object_for_tests, check_auth_mock_mocker):
    arguments = {"phone": "79175002040", "email": "stupnikov@otus.ru"}
    body = {
        "account": "horns&hoofs", "login": "admin", "method": "online_score", 'token': 'is_token',
        "arguments": arguments
    }
    response, code = api.method_handler(request={'body': body}, ctx={}, store=store_object_for_tests)
    assert code == api.OK
    score = response.get("score")
    assert score == 42


@pytest.mark.parametrize(
    'arguments',
    [{"client_ids": [1, 2, 3], "date": datetime.datetime.today().strftime("%d.%m.%Y")},
     {"client_ids": [1, 2], "date": "19.07.2017"},
     {"client_ids": [0]}]
)
@check_redis_host_for_test
def test_ok_interests_request(store_object_for_tests, check_auth_mock_mocker, arguments):
    body = {
        "account": "horns&hoofs", "login": "h&f", "method": "clients_interests", 'token': 'is_token',
        "arguments": arguments
    }
    context = {}
    response, code = api.method_handler(request={'body': body}, ctx=context, store=store_object_for_tests)
    assert code == api.OK
    assert len(arguments["client_ids"]) == len(response)
    assert (all(v and isinstance(v, list) and all(isinstance(i, str) for i in v)
                for v in response.values()))
    assert context.get("nclients") == len(arguments["client_ids"])
