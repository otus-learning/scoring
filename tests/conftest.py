import json
import os
import random
from unittest import mock

import pytest
import redis

from store import Store

check_redis_host_for_test = pytest.mark.skipif(
    os.environ.get('REDIS_HOST_FOR_TEST') is None,
    reason="REDIS_HOST_FOR_TEST environment variable not set "
)


@pytest.fixture
def bad_store_object():
    redis_inst = redis.Redis(host='localhost', port=666, password='точно не пароль')
    store = Store(external_store=redis_inst)
    return store


@pytest.fixture
def redis_instance_for_tests():
    redis_inst = redis.Redis(host=os.environ.get('REDIS_HOST_FOR_TEST'), decode_responses=True,
                             socket_timeout=1, socket_connect_timeout=1)
    interests_all = ["cars", "pets", "travel", "hi-tech", "sport", "music", "books", "tv", "cinema", "geek", "otus"]
    for id_ in [0, 1, 2, 3]:
        interests_random = random.sample(interests_all, 2)
        redis_inst.set(f'i:{id_}', json.dumps(interests_random))
    return redis_inst


@pytest.fixture
def store_object_for_tests(redis_instance_for_tests):
    store = Store(external_store=redis_instance_for_tests)
    return store


@pytest.fixture
def check_auth_mock_mocker():
    with mock.patch('api.check_auth', return_value=True):
        yield
