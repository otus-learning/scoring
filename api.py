#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
from datetime import datetime
import re
import logging
import hashlib
import uuid
from optparse import OptionParser
from http.server import HTTPServer, BaseHTTPRequestHandler

import redis

from scoring import get_score, get_interests
from store import Store
from utils import rewind_year

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class ValidationError(Exception):
    def __init__(self, message):
        if not isinstance(message, dict) and not isinstance(message, list):
            messages = [message]
        else:
            messages = message
        self.messages = messages
        Exception.__init__(self, messages)


class Field:

    def __init__(self, required=False, nullable=False, valid_values=None):
        self.required = required
        self.nullable = nullable
        self.valid_values = valid_values

    def deserialize(self, value):
        self.validate(value)
        return self._deserialize(value)

    def _deserialize(self, value):
        return value

    def validate(self, value):

        if value is None and not self.nullable:
            raise ValidationError('Field may not be null.')

        if self.valid_values and value not in self.valid_values:
            raise ValidationError('Not a valid choice.')

        return self._validate(value)

    def _validate(self, value):
        pass


class List(Field):

    def __init__(self, min_length: int = None, **kwargs):
        super(List, self).__init__(**kwargs)
        self.min_length = min_length

    def _validate(self, value):

        if not isinstance(value, list):
            raise ValidationError('value is not a list')

        if self.min_length:
            if len(value) < self.min_length:
                raise ValidationError(message=f'Shorter than minimum length {self.min_length}.')


class CharField(Field):

    def _validate(self, value):
        if not isinstance(value, str):
            raise ValidationError('Not a valid string.')


class ArgumentsField(Field):
    pass


class EmailField(CharField):

    def _validate(self, value):
        super(EmailField, self)._validate(value)
        if '@' not in value:
            raise ValidationError('Not a valid email address.')


class PhoneField(Field):

    def _validate(self, value):
        _value = str(value)
        if not re.fullmatch(r'7\d{10}', _value):
            raise ValidationError('Not a valid phone number.')

    def _deserialize(self, value):
        return str(value)


class DateField(Field):

    def _validate(self, value):
        try:
            datetime.strptime(value, '%d.%m.%Y').date()
        except (ValueError, TypeError):
            raise ValidationError('Not a valid date.')

    def _deserialize(self, value):
        return datetime.strptime(value, '%d.%m.%Y').date()


class BirthDayField(DateField):

    def _validate(self, value):
        super(BirthDayField, self)._validate(value)
        min_date = rewind_year(-70)
        date_birth_day = datetime.strptime(value, '%d.%m.%Y').date()
        if date_birth_day < min_date:
            raise ValidationError('Date of birth must be a date no more than 70 years old ')


class GenderField(Field):
    pass


class ClientIDsField(List):

    def _validate(self, value):
        super(ClientIDsField, self)._validate(value)
        if not all(isinstance(cid, int) for cid in value):
            raise ValidationError('value is not a client ids list')


class RequestMeta(type):
    def __new__(mcs, name, bases, attrs):
        field_list = []
        for key, value in list(attrs.items()):
            if isinstance(value, Field):
                value.name = key
                field_list.append(value)

        new_class = (super().__new__(mcs, name, bases, attrs))
        new_class.fields = field_list

        return new_class


class Request(metaclass=RequestMeta):

    def __init__(self):
        self.errors = {}

    def validate(self, data):
        self._validate(data)

    def _validate(self, data):
        pass

    def load(self, data):
        for field_obj in self.fields:
            raw_value = data.get(field_obj.name, None)

            if field_obj.required and field_obj.name not in data:
                self.errors[field_obj.name] = 'Missing data for required field.'

            if field_obj.name not in data and not field_obj.required:
                setattr(self, field_obj.name, None)
                continue

            value = field_obj.deserialize(raw_value)

            try:
                setattr(self, field_obj.name, value)
            except ValidationError as err:
                self.errors[field_obj.name] = err.messages

            if self.errors:
                raise ValidationError(self.errors)

        self.validate(data)


class ClientsInterestsRequest(Request):
    client_ids = ClientIDsField(required=True, min_length=1)
    date = DateField(required=False, nullable=True)


class OnlineScoreRequest(Request):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True, valid_values=[0, 1, 2])

    def _validate(self, data):
        required_pairs = [
            ('first_name', 'last_name'),
            ('email', 'phone'),
            ('birthday', 'gender'),
        ]

        check_pair: bool = False
        for pair in required_pairs:
            if pair[0] in data and pair[1] in data:
                check_pair = True

        if not check_pair:
            raise ValidationError('required pair')


METHOD_TO_SCHEMA_MAPPING = {
    'online_score': OnlineScoreRequest,
    'clients_interests': ClientsInterestsRequest
}


class MethodRequest(Request):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN


def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512((datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT).encode('utf-8')).hexdigest()
    else:
        digest = hashlib.sha512((request.account + request.login + SALT).encode('utf-8')).hexdigest()
    if digest == request.token:
        return True
    return False


def clients_interests(request_data: MethodRequest, method_data: ClientsInterestsRequest, ctx, store):
    response = {client: get_interests(store, client) for client in method_data.client_ids}
    ctx.update({'nclients': len(method_data.client_ids)})
    return response


def online_score(request_data: MethodRequest, method_data: OnlineScoreRequest, ctx, store):
    if request_data.is_admin:
        response = {'score': 42}
    else:

        response = {'score': get_score(
            store=store, phone=method_data.phone, email=method_data.email, birthday=method_data.birthday,
            gender=method_data.gender, first_name=method_data.first_name, last_name=method_data.last_name
        )}
        ctx.update({'has': [field.name for field in method_data.fields
                            if getattr(method_data, field.name, None) is not None]})

    return response


METHODS = {
    'clients_interests': clients_interests,
    'online_score': online_score
}


def method_handler(request, ctx, store):

    response, code = None, None

    try:
        request_data = MethodRequest()
        request_data.load(request['body'])

        if not check_auth(request_data):
            return None, FORBIDDEN

        method = request_data.method
        method_schema = METHOD_TO_SCHEMA_MAPPING[method]()
        data = request_data.arguments
        method_schema.load(data=data)

        response = METHODS[method](request_data, method_schema, ctx, store)
        code = 200

    except ValidationError as err:
        response, code = err.messages, INVALID_REQUEST

    return response, code


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    redis_inst = redis.Redis(
        host=os.environ.get('SCORING_REDIS_HOST', 'localhost'),
        socket_timeout=os.environ.get('SCORING_REDIS_TIMEOUT', 60),
        socket_connect_timeout=os.environ.get('SCORING_REDIS_TIMEOUT', 60),
        decode_responses=True,
    )
    store = Store(
        external_store=redis_inst,
        get_try_count=os.environ.get('SCORING_STORE_GET_TRY_COUNT', 1),
        get_try_delay=os.environ.get('SCORING_STORE_GET_TRY_DELAY', 10),
        cache_try_count=os.environ.get('SCORING_CACHE_TRY_COUNT', 1),
        cache_try_delay=os.environ.get('SCORING_CACHE_TRY_DELAY', 1),
    ),

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except Exception:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path]({"body": request, "headers": self.headers}, context, self.store)
                except Exception as e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(code, "Unknown Error"), "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r).encode())
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
